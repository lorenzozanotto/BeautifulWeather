//
// DateManager.swift
// BeautifulWeather
//
// Created by Lorenzo Zanotto on 23/12/2015.
// Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation

class DateManager {
    
    // Configuring the date
    let currentDate = NSDate()
    var dateFormatter = NSDateFormatter()
    
    var tomorrowDate: String {
        let tomorrow = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Day, value: 1,
            toDate: currentDate, options: NSCalendarOptions.init(rawValue: 0))
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return "\(dateFormatter.stringFromDate(tomorrow!)) 12:00:00"
        
    }
    
    var inTwoDaysDate: String {
        let tomorrowPlusOne = NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.Day, value: 2,
            toDate: currentDate, options: NSCalendarOptions.init(rawValue: 0))
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        print("*** IN TWO DAYS \(dateFormatter.stringFromDate(tomorrowPlusOne!))")
        return "\(dateFormatter.stringFromDate(tomorrowPlusOne!)) 12:00:00"
        
    }
    
    var inThreeDaysValue: String {
        let tomorrowPlusTwo = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 3,
            toDate: currentDate, options: NSCalendarOptions.init(rawValue: 0))
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        print("*** IN THREE DAYS \(dateFormatter.stringFromDate(tomorrowPlusTwo!))")
        return "\(dateFormatter.stringFromDate(tomorrowPlusTwo!)) 12:00:00"
        
    }
    
}