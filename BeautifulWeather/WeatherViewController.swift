//
//  ViewController.swift
//  BeautifulWeather
//
//  Created by Lorenzo Zanotto on 22/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import CoreData

class WeatherViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    // MARK: - Declaring Outlets
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var morningDegreesLabel: UILabel!
    @IBOutlet weak var morningWeatherWord: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var humidityPercentageLabel: UILabel!
    @IBOutlet weak var tomorrowDegreesLabel: UILabel!
    @IBOutlet weak var inTwoDaysDegreesLabel: UILabel!
    @IBOutlet weak var inThreeDaysDegreesLabel: UILabel!

    // Online data model
    var weatherModel = WeatherModel()
    var weatherArray: [SerializableWeatherData] = []
    var cityToShow: String!
    var cityToPass: String!
    
    // Offline data model
    var weatherSerializableModel: SerializableWeatherData!
    var fetchResultController: NSFetchedResultsController!
    var firstTimeSaving = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
        
            Performing the web request to the
            OpenWeather API and retrieve the results back
        
        */
        
        if cityToShow == nil {
            cityToPass = "Piancarani,it"
            cityToShow = "Piancarani,it"
        } else {
           // The user went to the other Scene and triggered the segue
           cityToPass = cityToShow
        }
        
        weatherModel.downloadWeatherData(cityToPass, completed: {
            print("**The data downloading is ended!")
            
            // Refreshing the content
            self.buttonRefresh("Type")
            
        })
        
            // We load weather data even though the fetch request is
            // successfully completed just to not show blank elements
            loadWeatherData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI() {
        morningDegreesLabel.text = weatherModel.degrees
        morningWeatherWord.text = weatherModel.weatherWord
        windSpeedLabel.text = weatherModel.windSpeed
        humidityPercentageLabel.text = weatherModel.humidityPercentage
        tomorrowDegreesLabel.text = weatherModel.tomorrowDegrees
        inTwoDaysDegreesLabel.text = weatherModel.inTwoDaysDegrees
        inThreeDaysDegreesLabel.text = weatherModel.inThreeDaysDegrees
        locationLabel.text = cityToShow
    }

    @IBAction func buttonRefresh(sender: AnyObject) {
        weatherModel.downloadWeatherData(cityToPass, completed: {
            print("**The data downloading is ended!")
            
            // Checking if values have been successfully received
            if ((self.weatherModel.degrees) != nil) {
                print(self.weatherModel.degrees)
                print(self.weatherModel.tomorrowDegrees)
                print(self.weatherModel.inTwoDaysDegrees)
                
                self.updateUI()
            } else {
                
                // Configuring the alert view showing that you're not connected
                let alertController = UIAlertController(title: "Oops... 😣", message: "You're not connected to internet, please do so to continue!", preferredStyle: .Alert)
                let alertOkAction = UIAlertAction(title: "Gotcha!", style: .Default, handler: nil)
                alertController.addAction(alertOkAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
            // Saving CoreData as soon as the Alamofire Fetching is completed
            self.saveWeatherData()
            
        })
        
    }
    
    // MARK: - CoreData Configurations and Methods

    func saveWeatherData() {
        
        if let managedObjectContext = (UIApplication.sharedApplication().delegate as? AppDelegate)?.managedObjectContext {
            weatherSerializableModel = NSEntityDescription.insertNewObjectForEntityForName("SerializableWeatherData", inManagedObjectContext: managedObjectContext) as! SerializableWeatherData
            
            weatherSerializableModel.degrees = morningDegreesLabel.text!
            weatherSerializableModel.weatherWord = morningWeatherWord.text!
            weatherSerializableModel.windSpeed = windSpeedLabel.text!
            weatherSerializableModel.humidityPercentage = humidityPercentageLabel.text!
            weatherSerializableModel.tomorrowDegrees = tomorrowDegreesLabel.text!
            weatherSerializableModel.inTwoDaysDegrees = inTwoDaysDegreesLabel.text!
            weatherSerializableModel.inThreeDaysDegrees = inThreeDaysDegreesLabel.text!
            weatherSerializableModel.firstTimeAdded = 2
            weatherSerializableModel.cityName = cityToPass!
            
            // That won't be the first saving time
            firstTimeSaving = false
            
            do {
                try managedObjectContext.save()
            } catch {
                print(error)
                return
            }
            
            print(weatherSerializableModel)
            self.firstTimeSaving = false
            
        }
        
    }
    
    func loadWeatherData() {
        
        let fetchRequest = NSFetchRequest(entityName: "SerializableWeatherData")
        let sortDescriptor = NSSortDescriptor(key: "degrees", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let manajedObjectContext = (UIApplication.sharedApplication().delegate as? AppDelegate)?.managedObjectContext {
            
            fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: manajedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            fetchResultController.delegate = self
            
            do {
                try fetchResultController.performFetch()
                weatherArray = fetchResultController.fetchedObjects as! [SerializableWeatherData]
            } catch {
                print(error)
            }
            
            for item in weatherArray {
                print("See what I have \(item.degrees)")
            }
            
            self.firstTimeSaving = false
            
        }
        
        // Load the Offline User Interface
        updateOfflineUI()
        
    }
    
    /*

        This updates the user interface when the app isn't connected
        to the internet, it displays previously fetched CoreData elements
    
    */
    func updateOfflineUI() {
        
        if weatherArray.count > 0 {
            morningDegreesLabel.text = weatherArray[weatherArray.count - 1].degrees
            morningWeatherWord.text = weatherArray[weatherArray.count - 1].weatherWord
            windSpeedLabel.text = weatherArray[weatherArray.count - 1].windSpeed
            humidityPercentageLabel.text = weatherArray[weatherArray.count - 1].humidityPercentage
            tomorrowDegreesLabel.text = weatherArray[weatherArray.count - 1].tomorrowDegrees
            inTwoDaysDegreesLabel.text = weatherArray[weatherArray.count - 1].inTwoDaysDegrees
            inThreeDaysDegreesLabel.text = weatherArray[weatherArray.count - 1].inThreeDaysDegrees
            locationLabel.text = weatherArray[weatherArray.count - 1].cityName
        }
        
        print("I executed the updateOfflineUI method")
    }
    
}

