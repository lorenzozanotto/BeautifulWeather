//
// JSONWeatherParser.swift
// BeautifulWeather
//
// Created by Lorenzo Zanotto on 22/12/2015.
// Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Alamofire

class JSONWeatherParser {
	
	// MARK: - Required Parsed Variables
	var cityName: String!
	var degrees: String!
	var weatherWord: String!
	var windSpeed: String!
	var humidityPercentage: String!
	
	var weatherModel: WeatherModel!
	
	// Dates
	var dateManager = DateManager()
	var tomorrowTemperature: String!
	var twoDaysTemperature: String!
	var threeDaystemperature: String!
	
	
	// MARK: - Parsing JSON and assigning it to a weather instance
	
	func parseAndAssignWeatherElements(resultValue: AnyObject) {
		print("**Let's begin parsing")
		
		// Custom dictionary
		if let dict = resultValue as? Dictionary<String, AnyObject> {
			
			// Retrieving City And Cordinates
			if let city = dict["city"] as? Dictionary<String, AnyObject> {
				if let name = city["name"] as? String {
					print(name)
					self.cityName = name
					
					if let coord = city["coord"] as? Dictionary<String, AnyObject> {
						if let lon = coord["lon"] as? Float {
							print(lon)
						}
					}
				}
			}
			
			/*
                
                "list" is the core component of all the OpenWeather API
                informations, we use this as root of our scraper and
                then we build all the substructures to parse the content      
            
            */
			if let list = dict["list"] as? [Dictionary<String, AnyObject>] {
				
				/*

				 The OpenWeatherAPI uses a list[] that contains a
				 weather[] so we need to explore both two and get
				 our final result

				 */
				
				if let weather = list[0] ["weather"] as? [Dictionary<String, AnyObject>] {
					if let mainDescription = weather[0] ["main"] as? String {
						print(mainDescription)
						self.weatherWord = mainDescription
					}
				}
				
				// Retrieving Degrees and Humidity
				if let mainInformations = list[0] ["main"] as? Dictionary<String, AnyObject> {
					if let temperature = mainInformations["temp"] as? Float {
						print(temperature)
						self.degrees = "\(String(format: "%.2f", temperature - 273))°"
					}
					if let humidity = mainInformations["humidity"] as? Int {
						print(humidity)
						self.humidityPercentage = "\(humidity)%"
					}
				}
				
				// Retrieving Wind Speed
				if let wind = list[0] ["wind"] as? Dictionary<String, AnyObject> {
					if let speed = wind["speed"] as? Double {
						print(speed)
						self.windSpeed = "\(speed) mph"
					}
				}
				
				/*

				 We don't know at what index the next day temperature will be found
				 so we need to iterate through all the list[] indexes to find the one
				 with our final parameter named "main" of "dt_txt" == "dateManager.X"
				 where X is the date we wanna parse

				 */
				
				// TOMORROW DEGREES
				var found = true
				var i = 0;
				while (found) {
					if let mainInformations = list[i] ["dt_txt"] as? String {
						if (mainInformations.rangeOfString(dateManager.tomorrowDate) != nil) {
							print(" Found \(mainInformations) at index \(i)")
							found = false
						} else {
							i++
							print("The counter is now \(i)")
						}
					}
				}
				
				// Scraping TOMORROW's degrees
				if let tomorrowInformations = list[i] ["main"] as? Dictionary<String, AnyObject> {
					if let tomorrowTemperature = tomorrowInformations["temp"] as? Float {
						print("I found tomorrow's temperature and it's \(tomorrowTemperature - 273)")
						self.tomorrowTemperature = "\(String(format: "%.2f", tomorrowTemperature - 273))°"
					}
				}
				
				// IN TWO DAYS DEGREES
				//
				//
				found = true
				i = 0
				while (found) {
					if let newInformations = list[i] ["dt_txt"] as? String {
						if (newInformations.rangeOfString(dateManager.inTwoDaysDate) != nil) {
							print(" Found \(newInformations) at index \(i)")
							found = false
						} else {
							i++
							print("The counter is now \(i)")
                            print(dateManager.inTwoDaysDate)
						}
					}
				}
				
				// Scraping TOMORROW's degrees
				if let tomorrowInformations = list[i] ["main"] as? Dictionary<String, AnyObject> {
					if let tomorrowTemperature = tomorrowInformations["temp"] as? Float {
						print("I found tomorrow's temperature and it's \(tomorrowTemperature - 273)")
						self.twoDaysTemperature = "\(String(format: "%.2f", tomorrowTemperature - 273))°"
                        print("TODAY IS \(NSDate())")
					}
				}
                
                // IN THREE DAYS DEGREES
                //
                //
                found = true
                i = 0
                while (found) {
                    if let newInformations = list[i]["dt_txt"] as? String {
                        if (newInformations.rangeOfString(dateManager.inThreeDaysValue) != nil) {
                            print("Found \(newInformations) at index \(i)")
                            found = false
                        } else {
                            i++
                            print("The counter is now \(i)")
                            print(newInformations)
                            print(dateManager.inThreeDaysValue)
                        }
                    }
                }
                
                // Scraping TOMORROW's degrees
                if let tomorrowInformations = list[i] ["main"] as? Dictionary<String, AnyObject> {
                    if let tomorrowTemperature = tomorrowInformations["temp"] as? Float {
                        print("I found tomorrow's temperature and it's \(tomorrowTemperature - 273)")
                        self.threeDaystemperature = "\(String(format: "%.2f", tomorrowTemperature - 273))°"
                    }
                }
				
			}
			
		}
		
	}
}





