//
// WeatherModel.swift
// BeautifulWeather
//
// Created by Lorenzo Zanotto on 22/12/2015.
// Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Alamofire

class WeatherModel {
	
	
	// MARK: - Generic Weather Informations
	var degreesInKelvin: Float!
	var weatherWord: String!
	var windSpeed: String!
	var humidityPercentage: String!
    var degrees: String!
	
	// MARK: - Specific Informations based on day time
	var tomorrowDegrees: String?
	var inTwoDaysDegrees: String?
	var inThreeDaysDegrees: String?
	
    // Other variables
    var city: String!
    var urlRequest: NSURL!
	
	// MARK: - Networking Logic
    func downloadWeatherData(cityName: String, completed: DownloadComplete) {
		
		// Configuring URLs
        var completeStringUrl = "\(BASE_URL)\("Piancarani,it")\(CONFIG_URL)"
        
        if !cityName.isEmpty {
           completeStringUrl = "\(BASE_URL)\(cityName)\(CONFIG_URL)"
        }
		
		let completeNSUrl = NSURL(string: completeStringUrl)!
		
		print("The url is \(completeStringUrl)")
		
		// Beginning the Web Request to the OpenWeatherAPI
		Alamofire.request(.GET, completeNSUrl).responseJSON(completionHandler: {
				
				response in
				
				let response = response.result
                print(response)
				
				// Initializing the Weather Parser
				let parser = JSONWeatherParser()
				
				if response.value != nil {
					parser.parseAndAssignWeatherElements(response.value!)
					
					// Assigning parsed values to the real ones
					self.degrees = parser.degrees
					self.weatherWord = parser.weatherWord
					self.windSpeed = parser.windSpeed
					self.humidityPercentage = parser.humidityPercentage
					self.city = parser.cityName
					self.tomorrowDegrees = parser.tomorrowTemperature
					self.inTwoDaysDegrees = parser.twoDaysTemperature
					self.inThreeDaysDegrees = parser.threeDaystemperature
				}
				
				// We execute the completed() -> () closure, passed
				// in this method's arguments to notify the system
				completed()
			})
	}
	
}







