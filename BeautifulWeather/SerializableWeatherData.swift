//
//  SerializableWeatherData.swift
//  BeautifulWeather
//
//  Created by Lorenzo Zanotto on 23/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import CoreData

class SerializableWeatherData: NSManagedObject {

    // MARK: - Generic Weather Informations
    @NSManaged var weatherWord: String
    @NSManaged var windSpeed: String
    @NSManaged var humidityPercentage: String
    @NSManaged var degrees: String
    @NSManaged var cityName: String
    
    // MARK: - Specific Informations based on day time
    @NSManaged var tomorrowDegrees: String
    @NSManaged var inTwoDaysDegrees: String
    @NSManaged var inThreeDaysDegrees: String
    
    // Reference counting for the Table Update
    @NSManaged var firstTimeAdded: Int
    
}
