//
//  SerializableCitiesData.swift
//  BeautifulWeather
//
//  Created by Lorenzo Zanotto on 24/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import CoreData

class SerializableCitiesData: NSManagedObject {
    
    @NSManaged var name: String!

}
