//
//  AddCityViewController.swift
//  BeautifulWeather
//
//  Created by Lorenzo Zanotto on 24/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import CoreData

class AddCityViewController: UIViewController {
    
    @IBOutlet weak var cityNameLabel: UITextField!
    var cityModel: SerializableCitiesData!
    
    @IBAction func closeButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func confirmButtonPressed(sender: AnyObject) {
        saveNewCityData()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func saveNewCityData() {
        
        if let managedObjectContext = (UIApplication.sharedApplication().delegate as? AppDelegate)?.managedObjectContext {
            
            cityModel = NSEntityDescription.insertNewObjectForEntityForName("SerializableCitiesData", inManagedObjectContext: managedObjectContext) as! SerializableCitiesData
            
            cityModel.name = cityNameLabel.text!
            
            do {
                try managedObjectContext.save()
                print("NEW CITY SAVED")
            } catch {
                print(error)
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

}
