//
//  Constants.swift
//  BeautifulWeather
//
//  Created by Lorenzo Zanotto on 22/12/2015.
//  Copyright © 2015 Lorenzo Zanotto. All rights reserved.
//

import Foundation

// MARK - Defining the constants to retrieve the OpenWeatherAPI URL

/*

    A working url for instance looks like this
    http://api.openweathermap.org/data/2.5/forecast?q=Piancarani,it&mode=json&appid=2de143494c0b295cca9337e1e96b00e0
*/

let BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?q="
// var CITY_URL = "Piancarani,it"
let CONFIG_URL = "&mode=json&appid=2de143494c0b295cca9337e1e96b00e0"

// Code to execute when the download request is completed
typealias DownloadComplete = () -> ()